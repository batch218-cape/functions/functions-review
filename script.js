// Functions Review

//1    2     3   (4) - size of array  
//0    1     2    3     4- indeces
let vowels = ["a", "e", "i", "o"]; 

// function - function keyword
// addVowel - function name 
    //For best practice, it is better to name our function in verb, to distinguish easily it's purpose
    //The function name should indicate the purpose of the function block 

                //parameters - is a variable where we store our values passed by argument
function addVowel(newVowel){ // newVowel = "u";

    // newVowel variable through parameter is accessible inside the function.
    vowels[vowels.length] = newVowel;
    console.log(vowels);

    //arrayName[index]
    //inside the square bracket must be an index / index number.
}
        
    //argument - argument/s is what we pass to parameter
addVowel("u"); //invocation //call 



function updateVowel(indexNum, newVowel){
    vowels[indexNum] = newVowel;
    console.log(vowels);
}
        //indexNum   //newVowel
updateVowel(1, "E");


let basketBallTeam = ["Rafael", "Enrico", "Ren", "Leonardo", "Jan Jonell"];

function addPlayer(player){
    basketBallTeam[basketBallTeam.length] = player; 
    console.log(basketBallTeam);
}

addPlayer("Joseph");

—-----------------------------------

    // Function Review

// let - keyword for non constrant variables
// vowels - array name (variable)
// a, e, i, o, u - elements
            //0     1     2     3  - indeces
let vowels = ["a", "e", "i" , "o"]; // this an example of array

// when in a name a function we usually named it using verb 

/*
    Syntax:
    
    function functionName(parameter){
        // code to execute when the function is called or invoked
    }

*/


// newVowel - parameter
function addVowel(newVowel){ // u
    // code to execute when calling or invoking a function
    vowels[vowels.length] = newVowel; // u
    console.log(vowels);
    
}

// invoke / call
// code inside the function will not execute/run if we don't invoke / call the function

// addVowel - functionName
// "u" - argument
addVowel("u");

// vowels = ["a", "e", "i" , "o", "u"];
function updateLastElementToUpperCase(elementToUpdate){
    if(elementToUpdate == "U"){ // the code inside the if statement will only run or executed if the returned value of the condition is true
        vowels[vowels.length-1] = elementToUpdate;
            let newElement =  vowels[vowels.length-1]
            let message1 = "Congratulations you have update your array";
            console.log(message1);
            return message1;
    }
    else if(elementToUpdate == "u"){
        let message2 = "You haven't update anything.";
        console.log(message2);
        return message2;
        
    }
    else{
        let message3 ="Your input (argument) doesn't match the last element";
        console.log(message3);
        return message3;
        
    }
}

let returnedMessage = updateLastElementToUpperCase("e");

console.log("vowels array after invocation of updateLastElementToUpperCase function: ")
console.log(vowels);
console.log("----------------");
console.log("2nd use of the value returned: ");
console.log("Proof that variables/values inside the function will be accessible using return: ");
console.log(returnedMessage);

            
            
            
            
            
            
            
            
            
            
            
            
            
            